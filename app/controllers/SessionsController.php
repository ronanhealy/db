<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
class SessionsController extends BaseController
 {
	public function create()
	{
		if(Auth::check())
		{
			
			return Redirect::to('admin');
		}
		
		return View::make('sessions/create');
	}
	
	public function store()
	{
		if (Auth::attempt(Input::only('email', 'password')))
		{
			return Redirect::to('admin');
			//return Auth::user();
		}
		
		return 'failed';
		
	}
	
	public function destroy()
	{
		Auth::logout();
		
		return Redirect::route('sessions.create');
	}
		
}
 