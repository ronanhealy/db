<?php

class BookController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$books = Book::all();


	return View::make('booklist')->with('books', $books); 
}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{		
			$book = Book::find($id);
			$author = $book->author;
			$allbooks = $author->books;
			return View::make('book')->with('book',$book)->with('author', $author)->with('allbooks',$allbooks);
		
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		 $book = Book::find($id); return View::make('book.edit')->with('book', $book); 

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function bookSearch()
	{
		$q = Input::get('query');
	
		$books = $this->book->whereRaw(
				"MATCH(title) AGAINST(? IN BOOLEAN MODE)",
				array($q)
		)->get();
	
		return View::make('books', compact('books'));
	
	}

}
