<?php

Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');

Route::resource('sessions', 'SessionsController');

Route::resource('book','BookController');

Route::get('admin', function()
{
	return View::make('admin');
	//return 'Admin Page';
})->before('auth');
//Route::get('users', 'UsersController@index');

//Route::get('users/{username}', 'UsersController@show');


Route::get('authors', 'AuthorController@listAuthors');
//second
Route::get('authors2', function ()
{
	$authors = Author::all();
	return View::make('authorslist')->with('authors', $authors);
});

Route::get('main', 'MainController@showMenu');

//Route::get('admin', 'AdminController@showList');

Route::get('/', function()
{
	User::create([
	'username' => 'Ronan Healy',
	'password' => 'super'

			]);

	return 'done';
});

Route::resource('users', 'UsersController');
/*Route::get('/', function (){

		$users = DB::table('users')->get();

		return $users;
		});*/
Route::get('books', function() {
	$books = Book::all();


	return View::make('booklist')->with('books', $books); });

Route::get('firstbook', 'BookController8@getFirst');

Route::post('books/search',array('as' => 'books.search','uses' => 'BookController@bookSearch'));


