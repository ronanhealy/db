<?php

class Review extends Eloquent
{

	public function book()
	{
		return $this->belongsTo('Book');
	}
}