<?php
class Book extends Eloquent {
	
	
	public function author()
	{
		
		return $this->belongsTo ( 'Author' );
	}
	
	public function bookings() {
		return $this->hasMany('Booking');
	}
	
	
}

?>
