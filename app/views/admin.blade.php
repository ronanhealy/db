@extends('layouts.default')

@section('content')	

	<h1>Librarian/Admin Section</h1>
	
	<div class="navbar">
	<div class="navbar-inner">
		<a id="logo" href="/">Library</a>
		<ul class="nav">
			<li><a href="/">Home</a></li>
			<li><a href="users">Review Members</a></li>
			<li><a href="books">Review Books</a></li>
			<li><a href="login"></a></li>
			<li><a href="logout">Logout</a></li>
		</ul>
	</div>
</div>
		
@stop
