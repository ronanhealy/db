@extends('layout') 

@section('header') 

Book List 

@stop


@section('content') 

@if(count($books) < 1)

<p>No book found at all!!!</p>


@elseif(count($books) == 1)

<p>Only a single book found!</p>


@else
<p>{{count($books)}} books were found.</p>

@endif 

@for ($i = 0; $i < count($books); $i++) 
{{{$books[$i]->title}}}
<br />

@endfor {{-- @foreach($books as $book)
	 
{{{$book->title}}}
<br />

@endforeach --}}
 @stop



