@section('content')
    <table border="1">
        <tr>
            <th>Name</th>
            <th>Date Of Birth</th>
            <th>Nationality</th>
        </tr>
    @foreach($authors as $author)
        <tr>
            <td>{{ $author->name }} </td>
            <td>{{ $author->date_of_birth }} </td>
            <td>{{ $author->nationality }} </td>
        </tr>
    @endforeach
    </table>
@stop