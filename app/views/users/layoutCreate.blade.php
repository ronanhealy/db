<html>
<head>

<link rel="stylesheet" type="text/css" href="css/create.css">

</head>

<body>
	<h1>@yield('header')</h1>

	<div id="leftMenu">
		@section('leftmenu')
		<p>
			<a href="{{{URL::to('admin')}}}">Home</a>
		</p>
		@show
	</div>

	@yield('content')
</body>
</html>
