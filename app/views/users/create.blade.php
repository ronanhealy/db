@extends('layouts.default')

@section('content')
	
	<h1> Create new Member</h1>

    {{ Form::open(['route' => 'users.store'])}}
	<div>
	{{ Form::label('username', 'Username: ')}}
	
	{{ Form::input('text', 'username') }}
	
	{{ $errors->first('username') }}
	
	</div>
	
	 <div>
	 {{ Form::label('password', 'Password: ')}}
	 {{ Form::password('password') }}
	 
	 {{ $errors->first('password') }}
	 </div>
	 
	 <p>{{ Form::label('address', 'Address: ') }}
  	{{ Form::text('address') }}</p>
  	
  	<p>{{ Form::label('contactNo', 'Phone:') }}
  		{{ Form::text('contactNo') }}</p>
	 
	 <div>
	 {{Form::submit('Create User') }}
	 </div>
	 
	{{ Form::close() }}
		
@stop




