@extends('layouts.default')

@section('content')	
	
	
	<h1> Library Member: {{ $user->username }}</h1>
	<br>
	<p>
	<p>
			<a href="{{{URL::to('users')}}}">Home</a>
		</p>
	<h2> Member Details </h2>
	Email: {{ $user->email }}
	<br>
	<p>
	Phone: {{ $user->phone }}
	<br>
	<p>
	Address: {{ $user->address }}
	<br>
	<p>
	User Confirmed: {{ $user->activated }}
	<br>
	<p>
	Books on Loan: {{ $user->books_on_loan }}
	<br>
	<p>
	Outstanding Fines: {{ $user->fines }}
	
	
@stop		