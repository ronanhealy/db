@extends('layout')

@section('header') 
 
 {{{$book->title}}} 
 @stop 
 
 @section('leftmenu')

@section('content')

<p>ISBN: {{{$book->isbn}}}</p>

<p>Published: {{{$book->publish_date}}}</p>

<em>Changes</em>

<p>Author: {{{$author->name}}}</p>

<p>All books by the author:</p>

<ul>
	@foreach($allbooks as $abook)
	<li>{{{$abook->title}}}</li>
	
	
	 @endforeach
</ul>
@stop

