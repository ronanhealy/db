<div class="search">
	    {{ Form::model(null, array('route' => array('books.search'))) }}
	    {{ Form::text('query', null, array( 'placeholder' => 'Search query...' )) }}
	    {{ Form::submit('Search') }}
	    {{ Form::close() }}
	</div>
