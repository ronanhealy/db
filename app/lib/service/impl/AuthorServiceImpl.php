<?php

namespace larkin\service\impl;

use larkin\service\AuthorService;
use larkin\repository\AuthorRepository;

class AuthorServiceImpl implements AuthorService {
	
	private $authorRepository;
	
	function __construct(AuthorRepository $authorRepository) 
	{
		
		$this->authorRepository = $authorRepository;
	}
	
	function getById($id)
    {
		return $this->authorRepository->getById ( $id );
	}
	
	function getAll() 
	{
		return $this->authorRepository->getAll ();
	}
}

