<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration {

/**
* Run the migrations.
*
* @return void
*/
public function up()
{
Schema::create('bookings', function($table){
            $table->increments('id');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('is_confirmed')->default(0);
            $table->timestamps();
        });
}

/**
* Reverse the migrations.
*
* @return void
*/
public function down()
{
Schema::drop('bookings');
}

}